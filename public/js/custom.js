$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});

$('#employeeList').DataTable();


$('#profile_image').change(function() { 
	$(".profile_picture").hide();
});

$('#country_id').on('change', function() {
var country_id = this.value;
$.ajax({
url: "/city-by-country",
type: "POST",
data: {
country_id: country_id
},
cache: false,
success: function(result){
$('#city_id').html('<option value="">Select City</option>'); 
$.each(result,function(key,value){
$("#city_id").append('<option value="'+value.id+'">'+value.name+'</option>');
}); 
}
});
});    

$('#createForm').on('submit',function(e){
	e.preventDefault();
	 $.ajax({
		url: $(this).attr("action"),
		type: $(this).attr("method"),
		dataType: "JSON",
		data: new FormData(this),
		processData: false,
		contentType: false,
		success: function (data, status)
        {
        	alert(data['message']);
        	window.location = '/employee';
 
        },
        error: function (xhr, desc, err)
        {
           alert("Required Filed are missing");

        }
	});

});

$(".delete_data").click(function(e){

   var id =  $(this).attr('data-id');
console.log(id);
if (confirm('are you sure you want to delete it')) {
     $.ajax({
		url: 'employee/'+$(this).attr('data-id'),
		type: 'DELETE',
		dataType: "JSON",
		data: {
                "id": $(this).attr('data-id'),
             "_token": $('meta[name="csrf-token"]').attr('content'),
                "_method": 'DELETE',
            },
		success: function (data, status)
        {
        	alert(data['message']);
        	window.location = '/employee';
 
        },
        error: function (xhr, desc, err)
        {
           alert("Something wend wrong");

        }
	});
} else {
    alert('Why did you press cancel? You should have confirmed');
}
})

  