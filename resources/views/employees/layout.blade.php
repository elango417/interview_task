<!DOCTYPE html>
<html>
<head>
    <title>Employee Details</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <style type="text/css">
    	img.profile_picture {
    		width: 50px;
    		padding-right: 5px;
		}

    </style>
</head>
<body style="background-color: #edf7ef">
  
<div class="container">
    <br/>
    <br/>
    <br/>
    @yield('content')
</div>
   
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>

</html>