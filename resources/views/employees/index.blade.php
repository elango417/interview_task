@extends('employees.layout')
 
@section('content')


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Employees Details</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('employee.create') }}"> Create New Employees</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
 
  <table id="employeeList" class="display">
    <thead>
        <tr>
            <th>Profile</th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>PinCode</th>
            <th>Age</th>
            <th>Degree</th>
            <th>City & Country</th>
            <th>Status </th>
            <th>Action </th>
        </tr>
    </thead>
    <tbody>
        @foreach($data['EmployeeList'] as $employeeData)
        <tr>
            <td><a href="{{$data["imageBaseUrl"].'/'. $employeeData->profile_picture}}" target="_blank"><img class="profile_picture" src='{{$data["imageBaseUrl"].'/'. $employeeData->profile_picture}}'></td>
            <td></a>{{$employeeData->name}}</td>
            <td>{{$employeeData->email}}</td>
            <td>{{$employeeData->address}}</td>
            <td>{{$employeeData->pincode}}</td>
            <td> @php  $age = (date('Y') - date('Y',strtotime($employeeData->dob)));@endphp {{$age}}</td>
            <td>{{$employeeData['educationInfo']['name']}}</td>
            <td>{{$employeeData['cityInfo']['name'] .'-'. $employeeData['countryInfo']['name']}} </td>
            <td>{{ ($employeeData->status == 1) ? 'Active': 'Inactive'  }}</td>
            <td>
        <a href="employee/{{$employeeData->id}}" class="btn btn-primary a-btn-slide-text">
        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
    </a>
       <a href="employee/{{$employeeData->id.'/edit'}}" class="btn btn-primary a-btn-slide-text">
        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
    </a>
    <span class="btn btn-primary a-btn-slide-text delete_data"  data-id="{{$employeeData->id}}">
       <span class="glyphicon glyphicon-remove " aria-hidden="true"></span>
    </span>

        </tr>
       @endforeach
    </tbody>
</table>

      
@endsection