@extends('employees.layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Employee Details</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('employee.index') }}"> Back</a>
        </div>
    </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('employee.update',$data['employeeList']['id']) }}" id= "createForm" method="PUT">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name" value="{{$data['employeeList']['name']}}" readonly>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{$data['employeeList']['email']}}" readonly>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>
                <textarea class="form-control" style="height:150px" name="address" placeholder="Address" readonly>{{$data['employeeList']['address']}}</textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Pin code:</strong>
                <input type="text" name="pincode" class="form-control" placeholder="Pin Code" value="{{$data['employeeList']['pincode']}}" readonly>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>DOB:</strong>
                <input type="date" name="dob" class="form-control" placeholder="Date Of Birth" value="{{$data['employeeList']['dob']}}" readonly>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Education:</strong>
                <select name="education_id" id="education_id" class="form-control" readonly>
                    <option value="">Select Degree</option>
                 @foreach($data['educationList'] as $dataInfo)
                    @if($dataInfo->id == $data['employeeList']['education_id'])
                        <option selected value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                    @else
                        <option value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                    @endif
                @endforeach       
                </select>
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Country:</strong>
                <select name="country_id" id="country_id" class="form-control" readonly>
                    <option value="">Select Country</option>
                 @foreach($data['countryList'] as $dataInfo)
                     @if($dataInfo->id == $data['employeeList']['country_id'])
                        <option selected value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                    @else
                        <option value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                    @endif
                @endforeach       
                </select>
                
        </div>
    </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>City:</strong>
                <select name="city_id" id="city_id" class="form-control" readonly>
                    <option value="">Select City</option>
                    @foreach($data['cityList'] as $dataInfo)
                        @if($dataInfo->id == $data['employeeList']['city_id'])
                            <option selected value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                        @else
                            <option value="{{$dataInfo->id}}">{{$dataInfo->name}}</option>
                        @endif
                @endforeach 
                </select>
                
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Profile Picture:</strong>
                <img class= "profile_picture" src ="{{$data['imageBaseUrl'].'/'.$data['employeeList']['profile_picture']}}">
                <a href = "{{$data['imageBaseUrl'].'/'.$data['employeeList']['profile_picture']}}" target="_blank"><img class= "profile_picture" src ="{{$data['imageBaseUrl'].'/'.$data['employeeList']['profile_picture']}}"> </a> 
                 
                          
            </div>
        </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                  <input readonly type="radio" id="active" name="status" value="1" {{($data['employeeList']['status'] == 1)? 'Checked':''}}>
  <label for="active">Active</label>
  <input readonly type="radio" id="inactive" name="status" value="0" {{($data['employeeList']['status'] == 0)? 'Checked':''}}>
  <label for="inactive">Inactive</label>
            </div>
        </div>
       
    </div>
   
</form>
@endsection