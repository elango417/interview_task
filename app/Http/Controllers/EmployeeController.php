<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\EducationList;
use App\CountryList;
use App\CityList;
use URL;
use Socialite;
use Google_Client;
use Google_Service_Directory;
use Config;
use Auth;



class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->employees = new Employee();
    }        
    protected const ACTIVE_FLAG = 1;

    public function index()
    { 
        $data['EmployeeList'] = $this->employees->getEmployeeDetails();
        $data['imageBaseUrl'] = URL::to('/images/');       
        return view('employees.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $educationList = EducationList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
        $countryList = CountryList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
       return view('employees.create',compact('educationList','countryList'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $validated = $request->validate([
            'name' => 'required','email' => 'required','address' => 'required','dob' => 'required','education_id' => 'required','city_id' => 'required','status' => 'required','profile_image' => 'required','pincode' => 'required'
        ]);
        
            
        if ($request->hasFile('profile_image')) {
            $image = $request->file('profile_image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            if($image->move($destinationPath, $name)){  
                $request['profile_picture'] =  $name;
            }
        }

       if(Employee::create($request->all())){
            return response()->json(array('status'=>true,'message'=>'Employee Added'));
       }else{
            return response()->json(array('status'=>false,'message'=>'something went wrong'));

       }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $data['employeeList'] = $this->employees->getEmployeeDetails($employee->id);
        $data['educationList'] = EducationList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
        $data['countryList'] = CountryList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
        $data['cityList'] = CityList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
          $data['imageBaseUrl'] = URL::to('/images/'); 
        return view('employees.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {

        $data['employeeList'] = $this->employees->getEmployeeDetails($employee->id);

        $data['educationList'] = EducationList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
        $data['countryList'] = CountryList::where('status',self::ACTIVE_FLAG)->select('id','name')->get();
        $data['cityList'] = CityList::where('status',self::ACTIVE_FLAG)->where('country_id',$data['employeeList']->country_id)->select('id','name')->get();
        $data['imageBaseUrl'] = URL::to('/images/'); 
        return view('employees.update',compact('data'));
      

        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    { 
        $validated = $request->validate([
            'name' => 'required','email' => 'required','address' => 'required','dob' => 'required','education_id' => 'required','city_id' => 'required','status' => 'required','pincode' => 'required'
        ]);

        if ($request->hasFile('profile_image')) {
            $image = $request->file('profile_image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            if($image->move($destinationPath, $name)){  
                $request['profile_picture'] =  $name;
            }
        }
        
        $input = $request->all();
        $resr = $employee->fill($input)->save(); 
        if($employee->fill($input)->save()){
            return response()->json(array('status'=>true,'message'=>'Employee Updated'));
       }else{
            return response()->json(array('status'=>false,'message'=>'something went wrong'));

       }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {


      if(Employee::find($employee->id)->delete()){
        return response()->json(array('status'=>true,'message'=>'Employee Deleted'));

      }else{
        return response()->json(array('status'=>false,'message'=>'something went wrong'));

      }
    }
    //Get city based on country id 
    public function getCity(Request $request){
        if(isset($request['country_id']) && !empty($request['country_id'])){
            $data = CityList::where('country_id',$request->input('country_id'))->select('id','name')->where('status',self::ACTIVE_FLAG)->get();
             return response()->json($data);
        }
    }
   
}
