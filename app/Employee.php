<?php

namespace App;
use App\EducationList;
use App\CountryList;
use App\CityList;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	use SoftDeletes;

	protected $table = 'employees';
	protected $fillable = ['name','email','address','dob','education_id','city_id','country_id','status','profile_picture','pincode'];
	protected $dates = ['deleted_at'];



	public function getEmployeeDetails($id = ''){

		 $employeeDetails =  Employee::select('id','name','email','address','dob','education_id','city_id','country_id','status','profile_picture','pincode')->with('educationInfo:id,name')->with('countryInfo:id,name')->with('cityInfo:id,name');
		 if(!empty($id)){
		 	return $employeeDetails = $employeeDetails->where('id',$id)->first();	
		 }else{

		 return $employeeDetails = $employeeDetails->get(); 
		 }
	}

	public function educationInfo(){
		return $this->hasOne('App\EducationList', 'id', 'education_id');
	}	

	public function countryInfo(){
		return $this->hasOne('App\CountryList', 'id', 'country_id');
	}
	public function cityInfo(){
		return $this->hasOne('App\CityList', 'id', 'city_id');
	}
}


